package com.dmitriy.azarenko.viewholder;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Дмитрий on 05.02.2016.
 */
public class Names implements Serializable {
    public String name;
    private String eMail;
    private int image;
    private String adress;
    private String phoneNum;

    public Names(String name, String eMail, int image) {
        this.name = name;
        this.eMail = eMail;
        this.image = image;

    }




    public String getName(){
        return name;
    }
    public void setName(String name){

        this.name = name;
    }
    public String getEmail(){
        return eMail;
    }
    public void setEmail(String eMail){

        this.eMail = eMail;
    }
    public int getImage(){
        return image;
    }
    public void setImage(int image){
        this.image = image;
    }


}

