package com.dmitriy.azarenko.viewholder;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ArrayList<Names> listOfNames = new ArrayList<>();
        for (int i = 0;i < 20;i++){

            listOfNames.add(new Names("Valera"+ i, "@mail",R.drawable.ic_trybka));
        }

        ListView listView = (ListView)findViewById(R.id.listView);
        listView.setAdapter(new MyAdapter(this, listOfNames));


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent mintent = new Intent(MainActivity.this,SecondActivity.class);
                Names names = listOfNames.get(position);
                mintent.putExtra("Key", names);
                startActivity(mintent);


            }
        });





    }

    class MyAdapter extends ArrayAdapter<Names> {

        public MyAdapter(Context context, ArrayList<Names> objects) {
            super(context, R.layout.list_item, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            View rowView = convertView;
            if (rowView == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                rowView = inflater.inflate(R.layout.list_item, parent, false);
                holder = new ViewHolder();
                holder.textView = (TextView) rowView.findViewById(R.id.nameId);
                holder.textViewEm = (TextView) rowView.findViewById(R.id.emailId);
                holder.imageView = (ImageView) rowView.findViewById(R.id.imageView);
                rowView.setTag(holder);
            } else {
                holder = (ViewHolder) rowView.getTag();
            }

            Names names = getItem(position);
            holder.textView.setText(names.getName());
            holder.textViewEm.setText(names.getEmail());
            holder.imageView.setImageResource(names.getImage());

            return rowView;
        }

        class ViewHolder {
            public ImageView imageView;
            public TextView textView;
            public TextView textViewEm;
        }


    }








}
