package com.dmitriy.azarenko.viewholder;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Дмитрий on 05.02.2016.
 */
public class SecondActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_activity);

        TextView nameText = (TextView)findViewById(R.id.nameTextid);
        TextView emailText = (TextView)findViewById(R.id.emailTextid);
        TextView adressText = (TextView)findViewById(R.id.adresTextid);
        TextView phoneText = (TextView)findViewById(R.id.phoneTextid);
        ImageView imageOnSec = (ImageView)findViewById(R.id.imageView2);


        Names names = (Names) getIntent().getExtras().getSerializable("Key");

        nameText.setText(names.getName());
        emailText.setText(names.getEmail());





    }
}
